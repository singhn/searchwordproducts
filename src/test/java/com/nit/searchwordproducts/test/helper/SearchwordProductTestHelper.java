package com.nit.searchwordproducts.test.helper;

import com.nit.searchwordproducts.orm.po.SearchWordProduct;

public class SearchwordProductTestHelper {
	public static SearchWordProduct getDefaultSearchWordProduct() {
		SearchWordProduct searchword = new SearchWordProduct();
		searchword.setName("melk");
		searchword.setUserId("1");
		searchword.setUserId("XYZ123");
		searchword.setSessionId("S1");

		return searchword;
	}
}

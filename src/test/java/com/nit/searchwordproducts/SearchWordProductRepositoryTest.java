package com.nit.searchwordproducts;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.nit.searchwordproducts.orm.po.SearchWordProduct;
import com.nit.searchwordproducts.orm.repository.SearchWordProductRepository;
import com.nit.searchwordproducts.test.helper.SearchwordProductTestHelper;

@ContextConfiguration(classes = SearchwordproductsApplication.class)
@DataMongoTest
@ExtendWith(SpringExtension.class)
@DirtiesContext
public class SearchWordProductRepositoryTest {
	private static final String DB_COLLECTION_NAME = "searchwordproduct";

	@Autowired
	SearchWordProductRepository repository;
	@Autowired
	MongoTemplate mongoTemplate;

	@DisplayName("empty array should be returned when no records in table.")
	@Test
	void checkEmptyArrayWithNoRecords() {
		repository.deleteAll();
		List<SearchWordProduct> searchwords = repository.findAll();

		assertThat(searchwords).hasSize(0);
	}

	@Test
	@DisplayName("only two records should be returned when two objects are in table.")
	void checkWithOneRecord() {
		repository.deleteAll();
		SearchWordProduct searchword = SearchwordProductTestHelper
				.getDefaultSearchWordProduct();
		mongoTemplate.save(searchword);
		searchword = SearchwordProductTestHelper.getDefaultSearchWordProduct();
		mongoTemplate.save(searchword);

		List<SearchWordProduct> searchwords = repository.findAll();

		assertThat(searchwords).hasSize(2);
	}

	@Test
	@DisplayName("no record should be returned when all records deleted.")
	void checkDeleteAll() {
		repository.deleteAll();
		List<SearchWordProduct> searchwords = repository.findAll();

		assertThat(searchwords).hasSize(0);
	}

	@Test
	@DisplayName("one record should be returned when the record is fetched with id.")
	void checkCreatedRecord() {
		SearchWordProduct searchword = SearchwordProductTestHelper
				.getDefaultSearchWordProduct();
		SearchWordProduct dbrecord = mongoTemplate.save(searchword);
		Optional<SearchWordProduct> fetchedRecord = repository
				.findById(dbrecord.getId());

		assertThat(fetchedRecord.get()).isNotNull();
	}

	@Test
	@DisplayName("no record should be returned when the deleted record is fetched with id.")
	void checkDeletedRecord() {
		SearchWordProduct searchword = SearchwordProductTestHelper
				.getDefaultSearchWordProduct();
		SearchWordProduct dbrecord = mongoTemplate.save(searchword);
		Optional<SearchWordProduct> fetchedRecord = repository
				.findById(dbrecord.getId());

		assertThat(fetchedRecord.get()).isNotNull();
		repository.deleteById(dbrecord.getId());
		fetchedRecord = repository.findById(dbrecord.getId());
		assertThat(fetchedRecord.orElse(null)).isNull();
	}

	@Test
	@DisplayName("one record should be returned when the created record is fetched with sesionid.")
	void checkGetBySessionIdRecord() {
		String sessionId = UUID.randomUUID().toString();
		SearchWordProduct searchword = SearchwordProductTestHelper
				.getDefaultSearchWordProduct();
		searchword.setSessionId(sessionId);
		SearchWordProduct dbrecord = mongoTemplate.save(searchword);
		List<SearchWordProduct> fetchedRecords = repository
				.findBySessionId(sessionId);

		assertThat(fetchedRecords).hasSize(1);
		assertThat(fetchedRecords.get(0).getId()).isEqualTo(dbrecord.getId());
		assertThat(fetchedRecords.get(0).getSessionId()).isEqualTo(sessionId);
	}

	@Test
	@DisplayName("record should be updated")
	void checkUpdatedRecord() {
		SearchWordProduct searchword = SearchwordProductTestHelper
				.getDefaultSearchWordProduct();
		SearchWordProduct dbrecord = mongoTemplate.save(searchword);
		SearchWordProduct fetchedRecord = repository.findById(dbrecord.getId())
				.orElse(null);

		assertThat(fetchedRecord).isNotNull();
		assertThat(fetchedRecord.getName()).isEqualTo(searchword.getName());

		fetchedRecord.setName("test_name");
		fetchedRecord.setSku("test_sku");
		fetchedRecord.setPurchased(true);
		fetchedRecord.setSessionId("test_session_id");
		fetchedRecord.setUserId("test_user_id");

		repository.save(fetchedRecord);
		fetchedRecord = repository.findById(fetchedRecord.getId()).orElse(null);
		assertThat(fetchedRecord).isNotNull();

		assertThat(fetchedRecord.getName()).isEqualTo("test_name");
		assertThat(fetchedRecord.getSku()).isEqualTo("test_sku");
		assertThat(fetchedRecord.getPurchased()).isEqualTo(true);
		assertThat(fetchedRecord.getSessionId()).isEqualTo("test_session_id");
		assertThat(fetchedRecord.getUserId()).isEqualTo("test_user_id");
		// creationdate should not be changed
		assertThat(fetchedRecord.getCreationDate()).isEqualTo(
				dbrecord.getCreationDate());
		// lastmodifieddate should be changed
		assertThat(fetchedRecord.getLastmodifiedDate()).isNotEqualTo(
				dbrecord.getLastmodifiedDate());
	}

}

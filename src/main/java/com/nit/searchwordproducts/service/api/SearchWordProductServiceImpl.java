package com.nit.searchwordproducts.service.api;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import com.nit.searchwordproducts.dto.MostSearchKeyword;
import com.nit.searchwordproducts.orm.po.SearchWordProduct;
import com.nit.searchwordproducts.orm.repository.SearchWordProductRepository;

@Service
public class SearchWordProductServiceImpl implements SearchWordProductService {

	private static final String ATTR_PURCHASED = "purchased";
	private static final String ATTR_SKU = "sku";
	private static final String ATTR_NAME = "name";
	private static final String ATTR_USER_ID = "userId";
	private static final String COUNT = "count";
	private static final String DB_COLLECTION_NAME = "searchwordproduct";
	private static final String CASE_INSENSITIVE = "i";
	private static final String MAPPING_ATTR_NAME = "name";

	@Autowired
	private SearchWordProductRepository repository;
	@Autowired
	private MongoTemplate template;

	@Override
	public SearchWordProduct getById(String id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	public SearchWordProduct create(SearchWordProduct product) {
		return repository.save(product);
	}

	@Override
	public SearchWordProduct update(SearchWordProduct product) {
		return repository.save(product);
	}

	@Override
	public void deleteById(String id) {
		repository.deleteById(id);
	}

	@Override
	public Optional<SearchWordProduct> getOptionalById(String id) {
		return repository.findById(id);
	}

	@Override
	public List<SearchWordProduct> getByUserId(String userid) {
		return repository.findByUserId(userid);
	}

	@Override
	public void deleteBySessionId(String id) {
		repository.deleteBySessionId(id);
	}

	@Override
	public List<String> getUserProductsByWord(String userid, String word) {
		GroupOperation groupByStateAndSumPop = Aggregation.group(ATTR_SKU)
				.count().as(COUNT);
		MatchOperation filterUser = Aggregation.match(Criteria.where(
				ATTR_USER_ID).is(userid));
		MatchOperation filterWord = Aggregation.match(Criteria.where(ATTR_NAME)
				.regex(word, CASE_INSENSITIVE));
		MatchOperation filterPurchased = Aggregation.match(Criteria.where(
				ATTR_PURCHASED).is(true));
		SortOperation sortByPopDesc = Aggregation.sort(Sort.Direction.DESC,
				COUNT);

		Aggregation aggregation = Aggregation.newAggregation(filterUser,
				filterWord, filterPurchased, groupByStateAndSumPop,
				sortByPopDesc, Aggregation.limit(5));
		AggregationResults<SearchWordProduct> result = template.aggregate(
				aggregation, DB_COLLECTION_NAME, SearchWordProduct.class);
		return result.getMappedResults().stream().map(w -> w.getId())
				.collect(Collectors.toList());
	}

	@Override
	public List<SearchWordProduct> getBySessionId(String id) {
		return repository.findBySessionId(id);
	}

	@Override
	public List<SearchWordProduct> getBySessionIdAndName(String id, String name) {
		return repository.findBySessionIdAndName(id, name);
	}

	@Override
	public void deleteBySessionIdAndName(String id, String name) {
		repository.deleteBySessionIdAndName(id, name);
	}

	@Override
	public boolean setPurchasedForSession(String id) {
		boolean success = false;
		List<SearchWordProduct> searchwords = getBySessionId(id);
		if (!searchwords.isEmpty()) {
			for (SearchWordProduct searchword : searchwords) {
				searchword.setPurchased(true);
			}
			repository.saveAll(searchwords);
			success = true;
		}
		return success;
	}

	@Override
	public boolean setUserIdForSession(String id, String userId) {
		boolean success = false;
		List<SearchWordProduct> searchwords = getBySessionId(id);
		if (!searchwords.isEmpty()) {
			for (SearchWordProduct searchword : searchwords) {
				searchword.setUserId(userId);
			}
			repository.saveAll(searchwords);
			success = true;
		}
		return success;
	}

	@Override
	public List<MostSearchKeyword> getMostSearchedKeyword() {
		GroupOperation groupByStateAndSumPop = Aggregation.group(ATTR_NAME)
				.first(ATTR_NAME).as(MAPPING_ATTR_NAME).count().as(COUNT);
		SortOperation sortByPopDesc = Aggregation.sort(Sort.Direction.DESC,
				COUNT);

		Aggregation aggregation = Aggregation.newAggregation(
				groupByStateAndSumPop, sortByPopDesc);
		AggregationResults<MostSearchKeyword> result = template.aggregate(
				aggregation, DB_COLLECTION_NAME, MostSearchKeyword.class);

		return result.getMappedResults();
	}

	@Override
	public List<MostSearchKeyword> getMostAddedProducts() {
		GroupOperation groupByStateAndSumPop = Aggregation.group(ATTR_SKU)
				.first(ATTR_SKU).as(MAPPING_ATTR_NAME).count().as(COUNT);
		SortOperation sortByPopDesc = Aggregation.sort(Sort.Direction.DESC,
				COUNT);

		Aggregation aggregation = Aggregation.newAggregation(
				groupByStateAndSumPop, sortByPopDesc);
		AggregationResults<MostSearchKeyword> result = template.aggregate(
				aggregation, DB_COLLECTION_NAME, MostSearchKeyword.class);

		return result.getMappedResults();
	}

	@Override
	public List<MostSearchKeyword> getProductsForSearchword(String word) {
		GroupOperation groupByStateAndSumPop = Aggregation.group(ATTR_SKU)
				.first(ATTR_SKU).as(MAPPING_ATTR_NAME).count().as(COUNT);
		MatchOperation filterWord = Aggregation.match(Criteria.where("name")
				.regex(word, CASE_INSENSITIVE));
		SortOperation sortByPopDesc = Aggregation.sort(Sort.Direction.DESC,
				COUNT);

		Aggregation aggregation = Aggregation.newAggregation(filterWord,
				groupByStateAndSumPop, sortByPopDesc);
		AggregationResults<MostSearchKeyword> result = template.aggregate(
				aggregation, DB_COLLECTION_NAME, MostSearchKeyword.class);
		return result.getMappedResults();
	}

	@Override
	public List<MostSearchKeyword> getSearchWordsForProduct(String sku) {
		GroupOperation groupByStateAndSumPop = Aggregation.group(ATTR_NAME)
				.first(ATTR_NAME).as(MAPPING_ATTR_NAME).count().as(COUNT);
		MatchOperation filterWord = Aggregation.match(Criteria.where(ATTR_SKU)
				.regex(sku, CASE_INSENSITIVE));
		SortOperation sortByPopDesc = Aggregation.sort(Sort.Direction.DESC,
				COUNT);

		Aggregation aggregation = Aggregation.newAggregation(filterWord,
				groupByStateAndSumPop, sortByPopDesc);
		AggregationResults<MostSearchKeyword> result = template.aggregate(
				aggregation, DB_COLLECTION_NAME, MostSearchKeyword.class);
		return result.getMappedResults();
	}

}

package com.nit.searchwordproducts.service.api;

import java.util.List;
import java.util.Optional;

import com.nit.searchwordproducts.dto.MostSearchKeyword;
import com.nit.searchwordproducts.orm.po.SearchWordProduct;

public interface SearchWordProductService {
	// general
	SearchWordProduct getById(String id);

	SearchWordProduct create(SearchWordProduct product);

	SearchWordProduct update(SearchWordProduct product);

	void deleteById(String id);

	Optional<SearchWordProduct> getOptionalById(String id);

	// user
	List<SearchWordProduct> getByUserId(String userid);

	List<String> getUserProductsByWord(String userid, String word);

	// session
	List<SearchWordProduct> getBySessionId(String id);

	boolean setPurchasedForSession(String id);

	void deleteBySessionId(String id);

	boolean setUserIdForSession(String id, String userId);

	List<SearchWordProduct> getBySessionIdAndName(String id, String name);

	void deleteBySessionIdAndName(String id, String name);

	// statistics
	List<MostSearchKeyword> getMostSearchedKeyword();

	List<MostSearchKeyword> getMostAddedProducts();

	List<MostSearchKeyword> getSearchWordsForProduct(String sku);

	List<MostSearchKeyword> getProductsForSearchword(String word);

}

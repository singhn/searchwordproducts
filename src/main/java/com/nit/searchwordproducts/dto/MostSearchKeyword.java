package com.nit.searchwordproducts.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MostSearchKeyword {
	private String name;
	private Integer count;
}

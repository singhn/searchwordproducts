package com.nit.searchwordproducts.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Valid
public class UserIDRequest {
	@NotNull
	@NotEmpty
	private String userId;

}

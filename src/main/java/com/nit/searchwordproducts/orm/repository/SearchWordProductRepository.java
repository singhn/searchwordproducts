package com.nit.searchwordproducts.orm.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nit.searchwordproducts.orm.po.SearchWordProduct;

public interface SearchWordProductRepository extends
		MongoRepository<SearchWordProduct, String> {

	List<SearchWordProduct> findByUserId(String userid);

	void deleteBySessionId(String sessionId);

	List<SearchWordProduct> findBySessionId(String id);

	List<SearchWordProduct> findBySessionIdAndName(String id, String name);

	void deleteBySessionIdAndName(String id, String name);
}

package com.nit.searchwordproducts.orm.po;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "searchwordproduct")
@Valid
@Getter
@Setter
@NoArgsConstructor
public class SearchWordProduct {
	@Id
	private String id;
	@Indexed(name = "user_id_index")
	@NotNull
	@NotEmpty
	private String userId;
	@NotEmpty
	private String name;
	private String sku;
	@Indexed(name = "session_id_index")
	private String sessionId;
	private Boolean purchased;
	private Boolean fromSuggestion;
	@CreatedDate
	private Date creationDate;
	@LastModifiedDate
	private Date lastmodifiedDate;

}

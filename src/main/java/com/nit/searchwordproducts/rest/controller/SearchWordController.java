package com.nit.searchwordproducts.rest.controller;

import io.swagger.v3.oas.annotations.Operation;

import java.net.URI;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.nit.searchwordproducts.dto.BindingErrorResponse;
import com.nit.searchwordproducts.orm.po.SearchWordProduct;
import com.nit.searchwordproducts.service.api.SearchWordProductService;

@RestController
public class SearchWordController {
	private static final String PATH_VARIABLE_ID = "id";
	private static final String PATH_ID = "/{id}";
	@Autowired
	private SearchWordProductService service;

	Logger logger = LoggerFactory.getLogger(SearchWordController.class);

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	@Operation(summary = "Create a new record for searchwordproduct")
	public ResponseEntity<SearchWordProduct> create(
			@Valid @RequestBody SearchWordProduct list,
			BindingResult bindingResult) {
		list.setId(null);
		BindingErrorResponse errors = new BindingErrorResponse();
		if (bindingResult.hasErrors()) {
			HttpHeaders headers = new HttpHeaders();
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return ResponseEntity.badRequest().headers(headers).build();
		}
		logger.debug("create request with data:{}", list);
		SearchWordProduct data = service.create(list);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
				.path(PATH_ID).buildAndExpand(data.getId()).toUri();
		return ResponseEntity.created(location).body(data);
	}

	@PutMapping(PATH_ID)
	@Operation(summary = "Updates the record with specific id")
	public SearchWordProduct update(
			@PathVariable(PATH_VARIABLE_ID) final String id,
			@RequestBody SearchWordProduct searchword) {
		searchword.setId(id);
		logger.debug("update request with data:{}", searchword);
		return service.update(searchword);
	}

	@GetMapping(PATH_ID)
	@Operation(summary = "Gets the record with specific id")
	public ResponseEntity<SearchWordProduct> get(
			@PathVariable(PATH_VARIABLE_ID) final String id) {
		Optional<SearchWordProduct> searchword = service.getOptionalById(id);
		logger.debug("get request for id:{}, data:{}", id, searchword);
		if (searchword.isPresent()) {
			return ResponseEntity.ok(searchword.get());
		}
		return ResponseEntity.notFound().build();
	}

	@DeleteMapping(PATH_ID)
	@Operation(summary = "Deletes the record with specific id")
	public ResponseEntity<SearchWordProduct> delete(
			@PathVariable(PATH_VARIABLE_ID) final String id) {
		logger.debug("delete request for id:{}", id);
		Optional<SearchWordProduct> searchword = service.getOptionalById(id);
		logger.debug("delete request for id:{}, searchwordd data:{}", id,
				searchword);
		if (searchword.isPresent()) {
			service.deleteById(id);
			return ResponseEntity.ok(searchword.get());
		}
		return ResponseEntity.notFound().build();

	}

}

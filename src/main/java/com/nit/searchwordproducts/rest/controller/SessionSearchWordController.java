package com.nit.searchwordproducts.rest.controller;

import io.swagger.v3.oas.annotations.Operation;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nit.searchwordproducts.dto.BindingErrorResponse;
import com.nit.searchwordproducts.dto.UserIDRequest;
import com.nit.searchwordproducts.orm.po.SearchWordProduct;
import com.nit.searchwordproducts.service.api.SearchWordProductService;

@RestController
@RequestMapping("/session")
public class SessionSearchWordController {
	private static final String PATH_VARIABLE_ID = "id";
	private static final String PATH_ID = "/{id}";
	private static final String PATH_PURCHASED = "/{id}/purchased";
	private static final String PATH_UPDATE_USERID = "/{id}/user";
	private static final String PATH_DELETE_SEARCH_WORD = "/{id}/{name}";
	@Autowired
	private SearchWordProductService service;

	Logger logger = LoggerFactory.getLogger(SessionSearchWordController.class);

	@PutMapping(PATH_PURCHASED)
	@Operation(summary = "Updates purchased attributes for all records associated with sessionid")
	public ResponseEntity setPurchased(
			@PathVariable(PATH_VARIABLE_ID) final String id) {
		logger.debug(
				"update purchased for all products associated with session id:{}",
				id);
		boolean success = service.setPurchasedForSession(id);
		if (success) {
			return ResponseEntity.ok().build();
		}

		return ResponseEntity.notFound().build();
	}

	@PostMapping(PATH_UPDATE_USERID)
	@Operation(summary = "Updates userid for all records associated with sessionid, this is used when you are switching from anonymous to loggedin user")
	public ResponseEntity setUserId(
			@PathVariable(PATH_VARIABLE_ID) final String id,

			@Valid @RequestBody UserIDRequest request,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			BindingErrorResponse errors = new BindingErrorResponse();
			HttpHeaders headers = new HttpHeaders();
			errors.addAllErrors(bindingResult);
			headers.add("errors", errors.toJSON());
			return ResponseEntity.badRequest().headers(headers).build();
		}
		logger.debug(
				"update userid for all products associated with session id:{}",
				id);
		boolean success = service.setUserIdForSession(id, request.getUserId());
		if (success) {
			return ResponseEntity.ok().build();
		}

		return ResponseEntity.notFound().build();
	}

	@GetMapping(PATH_ID)
	@Operation(summary = "Gets the list of all records associated with session id")
	public ResponseEntity<List<SearchWordProduct>> get(
			@PathVariable(PATH_VARIABLE_ID) final String id) {
		List<SearchWordProduct> searchwords = service.getBySessionId(id);
		logger.debug("get request for sessionid:{}, list data:{}", id,
				searchwords);
		if (!searchwords.isEmpty()) {
			return ResponseEntity.ok(searchwords);
		}
		return ResponseEntity.notFound().build();
	}

	@DeleteMapping(PATH_ID)
	@Operation(summary = "Deletes all records associated with session id")
	public ResponseEntity delete(@PathVariable(PATH_VARIABLE_ID) final String id) {
		logger.debug("delete request for id:{}", id);
		List<SearchWordProduct> searchwords = service.getBySessionId(id);
		logger.debug("delete request for id:{}, searchwordd data:{}", id,
				searchwords);
		if (!searchwords.isEmpty()) {
			service.deleteBySessionId(id);
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.notFound().build();

	}

	@DeleteMapping(PATH_DELETE_SEARCH_WORD)
	@Operation(summary = "Deletes all records associated with session id and searchword")
	public ResponseEntity deleteSearchWord(
			@PathVariable(PATH_VARIABLE_ID) final String id,
			@PathVariable("name") final String name) {
		logger.debug("delete request for id:{} and name:{}", id, name);
		List<SearchWordProduct> searchwords = service.getBySessionIdAndName(id,
				name);
		logger.debug("delete request for id:{}, searchwordd data:{}", id,
				searchwords);
		if (!searchwords.isEmpty()) {
			service.deleteBySessionIdAndName(id, name);
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.notFound().build();

	}

}

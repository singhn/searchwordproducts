package com.nit.searchwordproducts.rest.controller;

import io.swagger.v3.oas.annotations.Operation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nit.searchwordproducts.PathConstants;
import com.nit.searchwordproducts.orm.po.SearchWordProduct;
import com.nit.searchwordproducts.service.api.SearchWordProductService;

@RestController
@RequestMapping(PathConstants.PATH_USER_SEARCH_WORD_PRODUCT)
public class UserSearchWordProductController {
	private static final String PATH_VARIABLE_ID = "id";
	private static final String PATH_ID = "/{id}";

	@Autowired
	private SearchWordProductService service;

	Logger logger = LoggerFactory
			.getLogger(UserSearchWordProductController.class);

	@GetMapping(PATH_ID)
	@Operation(summary = "Gets the searchword products list with specific user id")
	public ResponseEntity<List<SearchWordProduct>> get(
			@PathVariable(PATH_VARIABLE_ID) final String userid) {
		List<SearchWordProduct> list = service.getByUserId(userid);
		logger.debug("get request for userid:{}, list data:{}", userid, list);
		return ResponseEntity.ok(list);
	}

	@GetMapping("/{id}/skus")
	@Operation(summary = "Gets the searchword list with specific user id")
	public ResponseEntity<List<String>> searchWithUserIdAndWord(
			@PathVariable(PATH_VARIABLE_ID) final String userid,
			@RequestParam("word") String word) {
		List<String> list = service.getUserProductsByWord(userid, word);
		logger.debug("get request for id:{}, list data:{}", userid, list);
		return ResponseEntity.ok(list);
	}

}

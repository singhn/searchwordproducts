package com.nit.searchwordproducts.rest.controller;

import io.swagger.v3.oas.annotations.Operation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nit.searchwordproducts.dto.MostSearchKeyword;
import com.nit.searchwordproducts.service.api.SearchWordProductService;

@RestController
@RequestMapping("/searchword/stats")
public class StatisticsSearchWordController {

	private static final String PATH_SKUS = "/skus";

	private static final String PATH_MOST_ADDED_SKUS = "/mostaddedskus";

	private static final String PATH_MOST_SEARCHED_KEYWORDS = "/mostsearchedkeywords";

	@Autowired
	private SearchWordProductService service;

	Logger logger = LoggerFactory
			.getLogger(StatisticsSearchWordController.class);

	@GetMapping(PATH_MOST_SEARCHED_KEYWORDS)
	@Operation(summary = "Gets the list of most searched keywords")
	public ResponseEntity<List<MostSearchKeyword>> getSearchedKeywords() {
		List<MostSearchKeyword> list = service.getMostSearchedKeyword();
		logger.debug("get request for mostsearchedkeywords data:{}", list);
		return ResponseEntity.ok(list);
	}

	@GetMapping(PATH_MOST_ADDED_SKUS)
	@Operation(summary = "Gets the list of most added products")
	public ResponseEntity<List<MostSearchKeyword>> getMostAddedProducts() {
		List<MostSearchKeyword> list = service.getMostAddedProducts();
		logger.debug("get request for mostaddedproducts data:{}", list);
		return ResponseEntity.ok(list);
	}

	@GetMapping(PATH_SKUS)
	@Operation(summary = "Gets the list of all product names associated with a word")
	public ResponseEntity<List<MostSearchKeyword>> getAllProductsForSearchKeyword(
			String word) {
		List<MostSearchKeyword> list = service.getProductsForSearchword(word);
		logger.debug("get request for getAllProductsForSearchKeyword data:{}",
				list);
		return ResponseEntity.ok(list);
	}

	@GetMapping("/words")
	@Operation(summary = "Gets the list of all search words associated with a product")
	public ResponseEntity<List<MostSearchKeyword>> getAllSearchwordsForProduct(
			String sku) {
		List<MostSearchKeyword> list = service.getSearchWordsForProduct(sku);
		logger.debug("get request for getAllSearchwordsForProduct data:{}",
				list);
		return ResponseEntity.ok(list);
	}

}

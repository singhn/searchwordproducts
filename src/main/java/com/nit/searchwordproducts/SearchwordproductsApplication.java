package com.nit.searchwordproducts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication
@EnableMongoAuditing
public class SearchwordproductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchwordproductsApplication.class, args);
	}

}
